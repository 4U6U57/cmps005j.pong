void setup() {
  size(800, 500);
  difficulty = width / 200;
  ballX = floor(random(height / 100));
  ballY = floor(random(width / 100));
  ballRight = true;
  ballDown = true;
}

void draw() {
  background(0);
  drawPaddle();
  drawBall();
}

int paddle;
void drawPaddle() {
  rectMode(CENTER);
  paddle = mouseY;
  //paddle = ballY;
  rect(width * 90 / 100, paddle, 10, height * 15 / 100);
}

int difficulty;
int ballX;
int ballY;
boolean ballRight;
boolean ballDown;
void drawBall() {
  rectMode(CENTER);
  rect(ballX, ballY, 10, 10);
  ballX += ballRight ? difficulty : -1 * difficulty;
  ballY += ballDown ? difficulty : -1 * difficulty;
  
  if((!ballRight && ballX < 5) || (ballRight && collision())) ballRight = !ballRight;
  if((!ballDown && ballY < 5) || (ballDown && ballY > height - 5)) ballDown = !ballDown;
}
boolean collision() {
  if((ballX + 5 > (width * 90 / 100) - 5) && (ballX + 5 < (width * 90 / 100) + 5)) {
    if((ballY > paddle - (height * 15 / 200)) && (ballY < paddle + (height * 15 / 200))) return true;
  }
  return false;
}
